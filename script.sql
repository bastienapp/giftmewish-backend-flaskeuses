create database wish_me_gift;
use wish_me_gift;

create user 'gudule'@'localhost' identified by '$2y$10$MDyzHgLFAvErn3qTL';
grant all privileges on wish_me_gift.* to 'gudule'@'localhost';
flush privileges;

#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: utilisateur
#------------------------------------------------------------

CREATE TABLE utilisateur(
        id_utilisateur Int  Auto_increment  NOT NULL ,
        nom            Varchar (50) NOT NULL ,
        mot_de_passe   Varchar (50) NOT NULL ,
        e_mail         Varchar (50) NOT NULL
	,CONSTRAINT utilisateur_PK PRIMARY KEY (id_utilisateur)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: liste_de_souhait
#------------------------------------------------------------

CREATE TABLE liste_de_souhait(
        id_liste_de_souhait Int  Auto_increment  NOT NULL ,
        theme               Enum ("liste_de_naissance","liste_anniversaire","liste_de_noel","liste_autre","liste_mariage") NOT NULL ,
        description         Text NOT NULL ,
        date_evenement      Date NOT NULL ,
        id_utilisateur      Int NOT NULL
	,CONSTRAINT liste_de_souhait_PK PRIMARY KEY (id_liste_de_souhait)

	,CONSTRAINT liste_de_souhait_utilisateur_FK FOREIGN KEY (id_utilisateur) REFERENCES utilisateur(id_utilisateur)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: cadeaux
#------------------------------------------------------------

CREATE TABLE cadeaux(
        id_cadeaux          Int  Auto_increment  NOT NULL ,
        description         Text NOT NULL ,
        lien                Text NOT NULL ,
        niveau_envie        Enum ("1","2","3","4","5") NOT NULL ,
        prix_approximatif   Float NOT NULL ,
        id_liste_de_souhait Int NOT NULL ,
        id_utilisateur_reserve      Int
	,CONSTRAINT cadeaux_PK PRIMARY KEY (id_cadeaux)

	,CONSTRAINT cadeaux_liste_de_souhait_FK FOREIGN KEY (id_liste_de_souhait) REFERENCES liste_de_souhait(id_liste_de_souhait)
	,CONSTRAINT cadeaux_utilisateur0_FK FOREIGN KEY (id_utilisateur_reserve) REFERENCES utilisateur(id_utilisateur)
)ENGINE=InnoDB;



# Ajout données :

# Pour utilisateur :
INSERT INTO utilisateur (nom, mot_de_passe, e_mail)
 VALUES
 ('Pilou', 'motdepasse123', 'pilou@email.com'),
('Jeanmi', 'motdepasse444', 'jm@email.com'),
('Belette', 'jadorelesanimaux', 'belette@email.com'),
('Scoubi', 'Samforever', 'scoub@email.com'),
('Tsar', 'xxxmdpcomplique', 'tsar@email.com'),
('Archi2', 'Gth154?', 'archi@email.com');

# Pour listes de souhaits (pour l'instant 5 listes de souhaits toutes pour le premier utilisateur) 
INSERT INTO liste_de_souhait (id_utilisateur, theme, date_evenement, description)
VALUES
(1, 'liste_de_noel', '2024-12-25', 'Pour la famille du côté de papa'),
(1, 'liste_de_naissance', '2024-05-18', 'Pour la fille de Maryam'),
(1, 'liste_anniversaire', '2024-04-12', 'Pour Marion'),
(1, 'liste_mariage', '2024-06-28', 'Pour Hugo et Ines'),
(1, 'liste_autre', '2024-09-09', 'Cremaillere de Cynthia');

# Pour les cadeaux (pour l'instant 3 cadeaux par liste, pour l'utilisateur 1)
INSERT INTO cadeaux (description, lien, niveau_envie, prix_approximatif, id_liste_de_souhait)
 VALUES
 ("Week-End pour deux personnes", "lien555", "5", "129", "1"),
 ("Saut en parachute", "lien777", "4", "158", "1"),
 ("Cuiseur à riz", "lien9", "5", "130", "1"),
 ("Canapé", "lien7", "3", "600", "2"),
 ("Netflix", "lien77", "5", "110", "2"),
 ("Port Aventura", "lien99", "2", "550", "2"),
 ("1 an de courses", "lien88", "1", "10000", "3"),
 ("1 semaine à Majorque", "lien66", "4", "1200", "3"),
 ("3 mois salle de sport", "lien33", "3", "99", "3"),
 ("Salon de jardin", "lien111", "2", "700", "4"),
 ("Journée au spa", "lien11", "5", "200", "4"),
 ("Vélo", "lien1", "1", "1300", "4");
