# Flask Template (lite)

- Suis les consignes du [brief de projet](https://gitlab.com/-/snippets/3637617).
- Travaille en [agilité avec le framework SCRUM](https://gitlab.com/-/snippets/3625937).
- Respecte les étapes [du GitLab Flow](https://gitlab.com/-/snippets/3625931).
- Crée [des demandes de fusion et procède à de la relecture de code](https://gitlab.com/-/snippets/3627568).

## Pré-requis

Installer Python.

## Initialisation du projet

Initialiser un environnement virtuel :

```bash
# - pour Linux et MacOS
python3 -m venv venv

# - pour Windows
python -m venv venv
```

Exécuter l'environnement virtuel :

```bash
# - pour Linux et MacOS
source venv/bin/activate

# - pour Windows
source venv/Scripts/activate
```

Pour installer les dépendances :

```bash
pip install -r requirements.txt
```

Lancement du projet :

```bash
flask run --debug
```

## Configuration du projet

Dupliquer le fichier `.env.sample` en `.env` et renseigner les valeurs attendues.
