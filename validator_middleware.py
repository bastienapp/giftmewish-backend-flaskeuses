# validator_middleware.py
from flask import jsonify, request
from functools import wraps

def validate_gift_data(next):
    @wraps(next)
    def wrapper(*args, **kwargs):
        data = request.json
        description = data.get('description')
        lien = data.get('lien')
        niveau_envie = data.get('niveau_envie')
        prix_approximatif = data.get('prix_approximatif')


        # Vérifier que la description, le lien, le niveau d'envie et le prix approximatif soient présents.
        if not all((description, lien, niveau_envie, prix_approximatif)):
            return jsonify({'message': 'La description, le lien, le niveau d\'envie et le prix approximatif sont obligatoires.'}), 400
        else:
    # Rest of your code goes here

            # passe au middleware suivant
            return next(*args, **kwargs)

    return wrapper

def validate_wishlist_data(next):
    @wraps(next)
    def wrapper(*args, **kwargs):
        data = request.json
        date_evenement = data['date_evenement']
        description = data['description']
        theme = data['theme']

        # Vérifier que le titre et la description soient présents.
        if not all((date_evenement, description, theme)):
            return jsonify({'message': 'La date de l\'évenement, la description et le thème sont obligatoires.'}), 400
        else:
            # passe au middleware suivant
            return next(*args, **kwargs)

    return wrapper
