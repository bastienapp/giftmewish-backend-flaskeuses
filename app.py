# Importations 
from flask import Flask, jsonify, request
from flask_mysqldb import MySQL
from dotenv import load_dotenv
from flask_bcrypt import Bcrypt

app = Flask(__name__)
bcrypt = Bcrypt(app)

load_dotenv()

app.config.from_object('config')

mysql = MySQL(app)

# importe les blueprints une fois la base de données initialisée
from users_controller import users_controller
app.register_blueprint(users_controller, url_prefix='/users')


if __name__ == '__main__':
    import os
    port = int(os.getenv('PORT')) if os.getenv('PORT') else 5000
    from waitress import serve
    serve(app, host="0.0.0.0", port=port)


#######      ROUTES      #######
    
   # Route pour créer un utilisateur avec son email et mdp :
@app.route('/register', methods=['POST'])
def register():
    #récupere les requetes en JSON(sur postman)
    data = request.get_json()
    e_mail = data.get('e_mail')
    mot_de_passe = data.get('mot_de_passe')
    if (e_mail is None or mot_de_passe is None):
    #erreur 401 = pas authorisé
        return jsonify({'error': 'Please specify both e_mail and mot_de_passe'}), 401

# hacher le mot de passe
    hashed_mot_de_passe = bcrypt.generate_password_hash(
        mot_de_passe
    ).decode('utf-8')
    print(hashed_mot_de_passe)
    # enregistrer le compte en base de données
    cursor = mysql.connection.cursor()
    cursor.execute(
        'INSERT INTO utilisateur (e_mail, mot_de_passe) VALUES (%s, %s)',
        (e_mail, hashed_mot_de_passe)
    )
    mysql.connection.commit()
    id_utilisateur = cursor.lastrowid

    # on retourne le compte créé (mais sans le mot de passe !)
    user_inserted = {
        'id_utilisateur': id_utilisateur,
        'e_mail': e_mail
    }
    return jsonify(user_inserted), 201

@app.route('/login', methods=['POST'])
def login():
    data = request.get_json()
    e_mail = data.get('e_mail')
    mot_de_passe = data.get('mot_de_passe')
    if (e_mail is None or mot_de_passe is None):
        return jsonify({'error': 'Please specify both e_mail and mot_de_passe'}), 401

    # Vérifier qu'un compte existe bien avec l'e_mail fourni.
    cursor = mysql.connection.cursor()
    cursor.execute(
        'SELECT * FROM utilisateur WHERE e_mail = %s',
        (e_mail,)
    )
    user = cursor.fetchone()

    if (user is None):
        return jsonify({'error': 'Invalid email'}), 401

    # Récupérer le mot de passe haché en base de données.
    # Note: il faut avoir la configuration suivante dans config.py pour que ça marche : MYSQL_CURSORCLASS = 'DictCursor'
    hashed_password = user['mot_de_passe']
    # Comparer avec bcrypt le mot de passe haché et le mot de passe fourni en clair.
    if (not bcrypt.check_password_hash(hashed_password, mot_de_passe)):
        return jsonify({'error': 'Invalid password'}), 401

    # Retourner le compte connecté (mais sans le mot de passe !).
    return jsonify({
        'id': user['id_utilisateur'],
        'e_mail': user['e_mail'],
    }), 200

