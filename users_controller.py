from flask import Blueprint, jsonify, request
from app import mysql
from validator_middleware import validate_gift_data
from validator_middleware import validate_wishlist_data

# cree un blueprint(plan)
users_controller = Blueprint('users_controller', __name__)


# Routes en GET :
# Affiche toutes les listes de souhaits d'un utilisateur :
@users_controller.route('/<int:id_user>/wishlists', methods=['GET'])
def get_wishlists_list(id_user):
    # Crée un curseur pour exécuter des requêtes SQL
    cursor = mysql.connection.cursor()
    # Sélectionne toutes les listes de souhaits de la table liste_de_souhait
    cursor.execute('SELECT * FROM liste_de_souhait WHERE id_utilisateur = %s', (id_user,)) # Soit une virgule après la variable, soit mettre la variable en str(variable) poru éviter une erreur "TypeError: 'int' object is not iterable"
    # Récupère tous les résultats de la requête
    wishlists = cursor.fetchall()
    
    # Vérification si les listes de souhait existent
    if wishlists:
        return jsonify(wishlists) # Convertit les résultats en JSON et les renvoie
    else:
        return f'Les listes de l\'utilisateur dont l\'id est le numéro {id_user} n\'ont pas été trouvées', 404

# Affiche une liste de souhait de l'utilisateur
@users_controller.route('/<int:id_user>/wishlists/<int:id_wishlist>', methods=['GET'])
def get_wishlist_list(id_user, id_wishlist):
    # Crée un curseur pour exécuter des requêtes SQL
    cursor = mysql.connection.cursor()
    # Sélectionne la liste de souhait dont l'id est celui fourni en url
    cursor.execute('SELECT * FROM liste_de_souhait WHERE id_liste_de_souhait = %s',(id_wishlist,))
    # Récupère le résultat de la requête
    wishlist = cursor.fetchone()

    # Vérification si la liste de souhait existe
    if wishlist:
        return jsonify(wishlist) # Convertit le résultat en JSON et le renvoie
    else:
        return f'La liste dont l\'id est le numéro {id_wishlist} n\'a pas été trouvée', 404

# Affiche tous les cadeaux de la liste d'un utilisateur
@users_controller.route('/<int:id_user>/wishlists/<int:id_wishlist>/gifts', methods=['GET'])
def get_wishlists_list_all_gifts(id_user, id_wishlist):
    
    cursor = mysql.connection.cursor()
    cursor.execute('SELECT * FROM cadeaux WHERE id_liste_de_souhait = %s', (id_wishlist,))
    gifts = cursor.fetchall()

    # Vérification si les cadeaux existent
    if gifts:
        return jsonify(gifts)
    else:
        return f'Les cadeaux de la liste de souhait dont l\'id est le numéro {id_wishlist} n\'ont pas été trouvés', 404
   
# Affiche un cadeau
@users_controller.route('/<int:id_user>/wishlists/<int:id_wishlist>/gifts/<int:id_gift>', methods=['GET'])
def get_gift(id_user, id_wishlist, id_gift):

    cursor = mysql.connection.cursor()
    cursor.execute('SELECT * FROM cadeaux WHERE id_cadeaux=%s', (id_gift, ))
    cadeau = cursor.fetchone()

    # Vérification si le cadeau existe
    if cadeau:
        return jsonify(cadeau)
    else:
        return f'Le cadeau dont l\'id est le numéro {id_gift} n\'a pas été trouvé', 404


    # Routes en POST : 
# Crée une liste de souhait chez un utilisateur
@users_controller.route('/<int:id_user>/wishlists', methods=['POST'])
@validate_wishlist_data
def create_wishlist(id_user):

    # Récupération des données depuis le corps/body de la requête reçue
    data = request.get_json()

    # Décomposition des données par information
    date_evenement = data['date_evenement']
    description = data['description'] 
    theme = data['theme']

    cursor = mysql.connection.cursor()
    # Insérer une nouvelle liste de souhaits dans la base de données
    cursor.execute('INSERT INTO liste_de_souhait (date_evenement, description, theme, id_utilisateur) VALUES (%s, %s, %s, %s)', (date_evenement, description, theme, id_user))
    mysql.connection.commit()

    return 'La liste de souhait a été ajoutée !', 201

# Crée un cadeau dans une liste d'un utilisateur
@users_controller.route('/<int:id_user>/wishlists/<int:id_wishlist>/gifts', methods=['POST'])
@validate_gift_data
def create_gift(id_user, id_wishlist):

    # Récupération des données depuis le corps/body de la requête reçue
    data = request.get_json()

    # Décomposition des données par information
    description = data['description']
    lien = data['lien']
    niveau_envie = data['niveau_envie']
    prix_approximatif = data['prix_approximatif']

    cursor = mysql.connection.cursor()
    # Insérer une nouvelle liste de souhaits dans la base de données
    cursor.execute('INSERT INTO cadeaux (description, lien, niveau_envie, prix_approximatif, id_liste_de_souhait) VALUES (%s, %s, %s, %s, %s)', (description, lien, niveau_envie, prix_approximatif, id_wishlist))
    mysql.connection.commit()

    return 'Le cadeau a été ajouté !', 201


    # Routes en PUT : 
# Modifie une liste
@users_controller.route('/<int:id_user>/wishlists/<int:id_wishlist>', methods=['PUT'])
@validate_wishlist_data
def put_wishlists_list(id_user, id_wishlist):

    # Récupération des données depuis le corps/body de la requête reçue
    data = request.get_json()

    # Décomposition des données par information
    date_evenement = data['date_evenement']
    description = data['description']
    theme = data['theme']

    cursor = mysql.connection.cursor()
    # Modifier une liste dans la BDD
    cursor.execute('UPDATE liste_de_souhait SET date_evenement = %s, description = %s, theme = %s WHERE id_liste_de_souhait = %s', (date_evenement, description, theme, id_wishlist))
    mysql.connection.commit()

    return 'La liste de souhait a bien été modifiée !', 200

# Modifie un cadeau
@users_controller.route('/<int:id_user>/wishlists/<int:id_wishlist>/gifts/<int:id_gift>', methods=['PUT'])
@validate_gift_data
def modify_gift(id_user, id_wishlist, id_gift):

    # Récupération des données depuis le corps/body de la requête reçue
    data = request.get_json()
    
    # Décomposition des données par information
    description = data['description']
    lien = data['lien']
    niveau_envie = data['niveau_envie']
    prix_approximatif = data['prix_approximatif']

    cursor = mysql.connection.cursor()
    # Modifier un cadeau dans la BDD
    cursor.execute('UPDATE cadeaux SET description = %s, lien = %s, niveau_envie = %s, prix_approximatif = %s  WHERE id_cadeaux = %s', (description, lien, niveau_envie, prix_approximatif, id_gift))

    mysql.connection.commit()
    return 'Le cadeau a bien été modifié !', 200


    # Routes en DELETE : 
# Supprime une liste et ses cadeaux
@users_controller.route('/<int:id_user>/wishlists/<int:id_wishlist>', methods=['DELETE'])
def delete_wishlist(id_user, id_wishlist):
    cursor = mysql.connection.cursor()
    cursor.execute("DELETE FROM cadeaux WHERE id_liste_de_souhait = %s", (id_wishlist,))
    cursor.execute("DELETE FROM liste_de_souhait WHERE id_liste_de_souhait = %s", (id_wishlist,))
    mysql.connection.commit()
    return 'La liste de souhaits et ses cadeaux ont bien été supprimés', 200

# Supprime un cadeau 
@users_controller.route('/<int:id_user>/wishlists/<int:id_wishlist>/gifts/<int:id_gift>', methods=['DELETE'])
def delete_present_from_a_wishlist_list(id_user, id_wishlist, id_gift):

    cursor = mysql.connection.cursor()
    # Supprime le cadeau de la base de données
    cursor.execute('DELETE FROM cadeaux WHERE id_cadeaux = %s', (id_gift, ))
    mysql.connection.commit()

    return 'Le cadeau a bien été supprimé.', 200